#!/bin/bash

run_number=$1

RUN_CFG_DIR=$LOCAL_RO_RUN_HOME/config
mkdir -p $RUN_CFG_DIR

TMP_CFG_DIR=/tmp/vfat_config/$run_number
mkdir -p $TMP_CFG_DIR
rsync -ra $LOCAL_RO_CFG_DIR $TMP_CFG_DIR
cd $TMP_CFG_DIR
tar -czf $RUN_CFG_DIR/$run_number.tar.gz .
echo "Config stored in $RUN_CFG_DIR/$run_number.tar.gz."
