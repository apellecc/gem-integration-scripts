import os
from pathlib import Path

bw_stream = os.popen("bwconfig --get=resource --type=flash")
bw_output = bw_stream.read()

bw_lines = bw_output.split("\n")
bw_dict = dict()
for l in bw_lines:
    try:
        key, val = l.split(" = ")
    except ValueError: continue
    bw_dict[key.replace("resource flash property ", "")] = val.replace("'", "")

fw_path = Path(bw_dict["HIL_FLASH_FILENAME_STR"])
fw_name = fw_path.name.replace(".bit", "")
if fw_name == "bitstream": # take the path of the folder instead
    fw_name = fw_path.parent.name
print(fw_name)
