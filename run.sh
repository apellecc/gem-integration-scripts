#!/bin/sh

echo "Saving runs in folder $LOCAL_RO_RUN_HOME..."
cd $LOCAL_RO_RUN_HOME

run_number=$(($(cat $LOCAL_RO_RUN_HOME/last.log)+1))
echo "Run number $run_number"
run_name=$(printf "%08d" $run_number)

NEW_RUN_FOLDER=$LOCAL_RO_RUN_HOME/online/$run_name

if [ -d $NEW_RUN_FOLDER ]; then
    echo "Run $run_name already exists! This means I lost track of the run count. Please contact the experts."
    exit
fi

# copy log file temporarily to log directory
cp $DAQ_SCRIPTS_PATH/template.log $LOCAL_RO_RUN_HOME/log/$run_name.log
run_log_path=$LOCAL_RO_RUN_HOME/log/$run_name.log

# Ask for user name:
read -p "Please write your name: " SHIFTER_NAME 
sed -i "s,\[SHIFTER_NAME\],$SHIFTER_NAME,g" $run_log_path
# Ask for run type:
RUN_TYPE=none
PS3="Choose the run type: (1-7): "
select RUN_TYPE in "physics" "multi-bx" "rate" "latency" "scan" "scurve" "test"; do
    break
done
if [ -z "${RUN_TYPE}" ]; then
    echo "Please choose a number from 1 to 7!"
    exit
fi

sed -i "s,\[RUN_TYPE\],$RUN_TYPE,g" $run_log_path
# Ask for run purpose:
read -p "Comment about run purpose (e.g. Trimming, overnight run, special gas etc.): " RUN_COMMENT
sed -i "s/\[RUN_COMMENT\]/$RUN_COMMENT/g" $run_log_path

# replace variables such as start date and fw version:
START_DATE=$(date +"%D %T")
sed -i "s,\[START_DATE\],$START_DATE,g" $run_log_path
FW_NAME=$(ssh $LOCAL_RO_BACKEND_SSH 'sha1sum '$LOCAL_RO_BACKEND_FW_PATH' | grep -o "^\w*\b"')
echo Firmware name: $FW_NAME
sed -i "s,\[FW_NAME\],$FW_NAME,g" $run_log_path

#OLD_META=$(ls -li $run_log_path)
#nano +10,0 $run_log_path
#NEW_META=$(ls -li $run_log_path)
#if [ "$NEW_META" = "$OLD_META" ]; then
#    # log not filled, delete file and exit
#    echo "Please fill the log file before starting the run."
#    rm $run_log_path
#    exit
#fi

#echo "Storing VFAT configuration for run..."
$DAQ_SCRIPTS_PATH/store_config.sh $run_name

#COUNTS_START=$(python3 $DAQ_SCRIPTS_PATH/read_l1a_count.py)
echo Starting run with name $run_name...
mkdir -p $NEW_RUN_FOLDER
$LOCAL_RO_GEMOS_INSTALL/bin/gem-dpdk --config $LOCAL_RO_CONF -o $NEW_RUN_FOLDER/run --max-events-per-file 10000

EVENT_COUNT=$(ssh $LOCAL_RO_BACKEND_SSH $LOCAL_RO_READ_L1A)
read -p "Local readout stopped. Press enter to continue."

STOP_DATE=$(date +"%D %T")
sed -i "s,\[STOP_DATE\],$STOP_DATE,g" $run_log_path

sed -i "s,\[EVENT_COUNT\],$EVENT_COUNT,g" $run_log_path
#LINES_COUNT=$(wc -l $run_log_path | awk '{print $1}')
#LINES_COUNT=$(($LINES_COUNT+1))
#nano +$LINES_COUNT,22 $run_log_path
sed -i '/^[[:blank:]]*#/d;s/#.*//' $run_log_path

# Create end of run JSON file to notify DQM:
touch $NEW_RUN_FOLDER/EoR.jsn

#if command -v telegram-send &> /dev/null
#then
#    telegram-send "Completed run $run_number
#$(cat $run_log_path)"
##Start time: $START_DATE
##End time: $STOP_DATE
##L1A count at the end: $EVENT_COUNT"
#fi

echo "Updating spreadsheet..."
python3 $DAQ_SCRIPTS_PATH/update_spreadsheet.py $run_number "$START_DATE" "$STOP_DATE" $EVENT_COUNT "$SHIFTER_NAME" "$RUN_TYPE" "$RUN_COMMENT" "$FW_NAME"

echo ""
cat $run_log_path

# For physics runs, quietly trigger reconstruction:
if [ $RUN_TYPE = "physics" ]; then
    echo "Starting reconstruction for physics run..."
    dtach -n /tmp/reco-$run_name $LOCAL_RO_RECO_COMMAND $run_name --alert 
elif [ $RUN_TYPE = "multi-bx" ]; then
    echo "Starting reconstruction for multi-bx run..."
    dtach -n /tmp/reco-$run_name $LOCAL_RO_RECO_COMMAND $run_name --alert  --multi-bx
else
        if [ $LOCAL_RO_DQM_SAFE = true ]; then
	    echo "Concatenating files only, then leaving..."
	    RECO_DIR=/data/ge21/qc8/runs
	    cat $NEW_RUN_FOLDER/run-be\:fe\:00\:00\:00\:0a-index000* > $RECO_DIR/compressed/${run_number}a.raw.zst
	    cat $NEW_RUN_FOLDER/run-be\:fe\:00\:00\:00\:0b-index000* > $RECO_DIR/compressed/${run_number}b.raw.zst
	    rm -rf $NEW_RUN_FOLDER/run-be*
        fi
fi

echo ""
echo "---------------------------------------------------------------------------"
echo "Data taking for run $run_number finished."
echo "Please update the run spreadsheet with the missing details."
echo "---------------------------------------------------------------------------"

echo $run_number > $LOCAL_RO_RUN_HOME/last.log
