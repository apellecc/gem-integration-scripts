import argparse
import os
import requests
import sys
import random

import gspread
from gspread.exceptions import SpreadsheetNotFound

SPREADSHEET_NAME = os.environ["LOCAL_RO_SPREADSHEET"]
MM_URL = os.environ["LOCAL_RO_MATTERMOST_URL"]

""" Return column based on the content of the cell in the first row """
def find_column(worksheet, name):
    cells = worksheet.findall(name)
    for cell in cells:
        if cell.row==1:
            return cell.col
    raise IndexError("No column with name {} found in spreadsheet".format(name))

""" Return cell based on the content but only in column with the chosen header """
def find_cell_in(worksheet, colname, content):
    column = find_column(worksheet, colname)
    cells = worksheet.findall(content)
    for cell in cells:
        if cell.col==column:
            return cell
    raise IndexError("No cell with content {} found in column {}".format(content, colname))


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("run_number", type=int)
    parser.add_argument("time_start", type=str)
    parser.add_argument("time_end", type=str)
    parser.add_argument("triggers", type=str)
    parser.add_argument("shifter", type=str)
    parser.add_argument("run_type", type=str)
    parser.add_argument("run_comment", type=str)
    parser.add_argument("fw_name", type=str)
    args = parser.parse_args() 

    """ Open the document: """
    gc = gspread.service_account()
    try:
        sh = gc.open(SPREADSHEET_NAME)
    except SpreadsheetNotFound:
        print("Could not find spreadsheet named {}, presumably because the spreadsheet name has changed.".format(SPREADSHEET_NAME))
        print("This run has not been logged. Please inform the developers now.")
        sys.exit(1)
    worksheet = sh.get_worksheet(0)

    """ Find the row of the previous run, but only in the run column: """
    cell = find_cell_in(worksheet, "Run", str(args.run_number-1))

    #results_url = "https://me0pro.web.cern.ch/gem/testbeam/april2023/analysis/{:08d}".format(args.run_number)
    run_row = cell.row+1
    worksheet.update_cell(cell.row+1, cell.col-1+find_column(worksheet, "Run"), str(args.run_number))
    worksheet.update_cell(cell.row+1, cell.col-1+find_column(worksheet, "Start"), args.time_start)
    worksheet.update_cell(cell.row+1, cell.col-1+find_column(worksheet, "Stop"), args.time_end)
    worksheet.update_cell(cell.row+1, cell.col-1+find_column(worksheet, "L1A count"), args.triggers)
    worksheet.update_cell(cell.row+1, cell.col-1+find_column(worksheet, "Type"), args.run_type)
    worksheet.update_cell(cell.row+1, cell.col-1+find_column(worksheet, "Comment"), args.run_comment)
    worksheet.update_cell(cell.row+1, cell.col-1+find_column(worksheet, "Shifter"), args.shifter)
    worksheet.update_cell(cell.row+1, cell.col-1+find_column(worksheet, "Firmware SHA1"), args.fw_name)

    print("Spreadsheet updated.")

    try: 
        mm_message = "Run {} of type \"{}\" started by {} just ended with {} events".format(args.run_number, args.run_type, args.shifter, args.triggers)

        # retrieve inspirational quote
        quote_api_url = "https://zenquotes.io/api/random"
        quote_response = requests.get(quote_api_url)
        if quote_response.status_code == requests.codes.ok:
            quote = quote_response.json()[0]
            mm_message += "\n"
            jackpot = False
            if random.randint(0,10)>9: jackpot = True
            if jackpot: mm_message += "«"
            mm_message += "\"{}\" -- {}".format(quote["q"], quote["a"])
            if jackpot:
                mm_message += "»"
                mm_message += " -- Michael Scott"

        mm_request = requests.post(MM_URL, json={"text": mm_message})
        with open("/tmp/outcome.txt", "w") as f: f.write("All right!\n")

    except Exception as e:
        # fail silently, but let me know...
        with open("/tmp/outcome.txt", "w") as f:
            f.write("Something wrong: ")
            f.write(str(e))
            f.write("\n")

if __name__=="__main__":
    main()
