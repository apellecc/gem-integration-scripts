# GEM DAQ integration scripts

Collection of utilities to assist the data taking in GEM integration setups.

## Installation

1. Clone the repository
2. `cd` to the repository path
3. Copy the file `examples/config.sh` to the main repository folder and edit it according to your configuration:
    - `DAQ_SCRIPTS_PATH` to the main repository folder
    - `GEMOS_INSTALL` to the installation of `cmsgemos` you are using
    - `GEMHARDWARE_INSTALL` to the installation folder of the `gemhardware` subdirectory of `cmsgemos`
    - `RUN_HOME` to the directory where you want to store the run files
4. Run `sh install.sh` and restart your shell session.

## Configuration

To be able to start the local readout as regular user, you need to assign the `gem-dpdk` executable ownership to the `root` user and to enable the special user permission flag for it:
```bash
sudo chown root:root $GEMOS_INSTALL/bin/gem-dpdk
sudo chmod u+s $GEMOS_INSTALL/bin/gem-dpdk
```

## Usage

To start the local readout during a run, use the `start-run` command.
