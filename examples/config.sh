export DAQ_ENV_HOME=$HOME/me0-stack-ng
export DAQ_SCRIPTS_PATH=$DAQ_ENV_HOME/gem-integration-scripts
export LOCAL_RO_GEMOS_INSTALL=$DAQ_ENV_HOME/cmsgemos/_build/_install # path where the cmsgemos client executables are installed
export LOCAL_RO_BACKEND_SSH=gempro@eagle60 # username at hostname of the backend board
export LOCAL_RO_BACKEND_FW_PATH=etc/cmsgemos/backend-firmware/bitstream.bit # path of the backend firmware bitstream
export LOCAL_RO_READ_L1A='bash -c "LD_LIBRARY_PATH=/mnt/persistent/lib/:/mnt/persistent/gempro/lib/ PYTHONPATH=/mnt/persistent/gempro/lib python me0-stack/read_register.py BEFE.GEM.TTC.L1A_ID"'
export LOCAL_RO_GEMHARDWARE_INSTALL=$LOCAL_RO_BACKEND_SSH:/mnt/persistent/gempro/ # path where the gemhardware is installed on the back-end board
export LOCAL_RO_RUN_HOME=/mnt/data/integration/me0stack # data where all the run directories are stored
export LOCAL_RO_DQM_SAFE=false # if true, automatically concatenate non-physics runs as soon as they end and delete the parts files
export LOCAL_RO_CONF=/home/gempro/me0-stack-ng/config/local-readout.toml
export LOCAL_RO_CFG_DIR="$LOCAL_RO_GEMHARDWARE_INSTALL/etc/cmsgemos/vfat" # path(s) on the backend board where the VFAT configuration is stored
export LOCAL_RO_SPREADSHEET="ME0 stack runs" # name of the spreadsheets where to save the run list
export LOCAL_RO_MATTERMOST_URL="https://mattermost.web.cern.ch/hooks/pgcddgcge7nfxrxcyw73mhw3ro" # mattermost bot url to notify after each run
export LOCAL_RO_RECO_COMMAND="reco-me0"
